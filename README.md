# Devops Test 01

## Dependencies to run locally
- Kubectl
- Homebrew
- Helm v3, `brew install helm` or `brew upgrade helm`
- Helmfile, `brew install helmfile`
- Helm-diff plugin, `helm plugin install https://github.com/databus23/helm-diff`

## Instructions to run on `Docker for Mac` k8s cluster
- `./scripts/docker-build`
- `./scripts/install-helm-charts`
- `./scripts/kubectl-apply-all`

## Improvements
### Flask App:
- Used pycryptodome instead of deprecated pycrypto package. 
- Endpoints now expect data in JSON format.
- Waitress as backend server if environment is set to production.
- Added checks for empty values, and empty environment variables.

After the k8s cluster starts (a load balancer listens on port 6000 by default):

- encrypt: `curl -H "Content-Type: application/json" -d '{"raw":"test"}' -X POST http://localhost:6000/encrypt`
- returns: `d/JVzAjpOzVB9AVli+Tb4dbnFlbPKaKLT2wXIFbyP5g=`
- decrypt: `curl -H "Content-Type: application/json" -d '{"enc":"d/JVzAjpOzVB9AVli+Tb4dbnFlbPKaKLT2wXIFbyP5g="}' -X POST http://localhost:6000/decrypt`
- returns: `test`

### Helm Charts:
- App Helm Chart:
    - Implemented the flask application as a helm chart under `kubernetes/app-chart` folder. This lets dynamic configuration for k8s yaml files.
    - So that values like PORT, AES key can be passed using helm, e.g. `helm install chart --set aeskey=somekey1234`.

- Other Helm Charts:
    - Because i didn't have too much time, helm charts was the quickest way to implement monitoring and logging solutions.
    - Added a default dashboard to Grafana chart by overriding chart values, using a dashboard from https://grafana.com/grafana/dashboards.
    
    ![](https://raw.githubusercontent.com/ozankasikci/ozankasikci.github.io/master/k8s/grafana.png)

    - Used `Metrics server` to power `kubectl top` and `kubernetes-dashboard`.
    - Used Prometheus for monitoring events and alerting.
   
### Kubernetes:
- Implemented EFK stack for logging, and instead of using a helm chart, manually written all the config files to keep it more flexible.
- Fluentd is implemented as a Daemonset and rotates logs from all the containers to ES.
- Kibana can be used to search and visualize the collected logs.

![](https://raw.githubusercontent.com/ozankasikci/ozankasikci.github.io/master/k8s/kibana.png)

- Kubernetes dashboard is also implemented with raw YAML because currently there is no helm chart for dashboard v2, which has better metrics-server integration.

![](https://raw.githubusercontent.com/ozankasikci/ozankasikci.github.io/master/k8s/dashboard.png)

### Scripts:
- Added separate scripts for port-forwarding pods on localhost. They looked a bit boring so added some colored output! They should be run on separate shells sessions.
- `docker-build`, `install-helm,charts`, `kubectl-apply-all` are being used on both local development environment and Gitlab CI.

### Gitlab CI Integration:
- On each commit CI/CD pipeline gets triggered and starts a simple `build` -> `deploy` pipeline.
- Utilizing the same scripts i've used on local development environment, a docker image gets built and pushed to the docker.hub repository.
- Then using Gitlab environment variables, deploy job gets executed, connects to k8s cluster and runs deploy scripts.

![](https://raw.githubusercontent.com/ozankasikci/ozankasikci.github.io/master/k8s/gitlab.png)

## What could be improved?
- After testing the Gitlab CI integration i've realized that the ES storage won't work on AWS or other cloud providers. If i had time i'd have fixed that using aws-ebs storage class on production env.
- Could have used a separate user for Gitlab with proper RBAC config cluster instead of using admin user.
