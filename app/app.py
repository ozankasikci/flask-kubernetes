from flask import Flask, request
from Crypto.Cipher import AES
from Crypto import Random
import base64
import os
import sys
import logging

mandatory_env_vars = ["APP_AES_KEY", "PORT"]

logging.basicConfig(filename='app.log', level=logging.DEBUG)

log = logging.getLogger()

encrypt_key = os.getenv('APP_AES_KEY')
port = os.getenv('PORT')

app = Flask(__name__)

BS = 16


def pad(s): return s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def unpad(s): return s[:-ord(s[len(s)-1:])]


def encrypt(raw):
    raw = pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(encrypt_key.encode(), AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw.encode()))


def decrypt(enc):
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cipher = AES.new(encrypt_key.encode(), AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))


@app.route('/encrypt', methods=['POST'])
def encrypt_route():
    req_data = request.get_json()
    raw = req_data.get("raw")

    if not raw:
        return "No value given to encrypt", 400

    log.info(f"Encrypting {raw}")
    return encrypt(raw)


@app.route('/decrypt', methods=['POST'])
def decrypt_route():
    req_data = request.get_json()
    enc = req_data.get("enc")

    if not enc:
        return "No value given to decrypt", 400

    log.info(f"Decrypted {decrypt(enc)}")
    return decrypt(enc)


if __name__ == '__main__':
    for var in mandatory_env_vars:
        if var not in os.environ:
            print("Please set the environment variable {}".format(var))
            sys.exit(1)

    print(f"Listening on port {port}")

    if os.environ.get("FLASK_ENV") == "production":
        from waitress import serve
        serve(app, host="0.0.0.0", port=port)
    else:
        app.run(host='0.0.0.0', port=port)
